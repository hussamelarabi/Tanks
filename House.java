package A3_GAME;

public class House extends Sprite implements Runnable {

    private boolean houseStatus;//Created/Destroyed
    private double vel;
    private int accuracy;
    private double direction;
    private Tank tank;//the tank to be followed by the house
    public Missile missile = new Missile(0, 0, 0, 0, false);
    private Client client;

    public House(double x, double y, int width, int height, double vel, int accuracy, Tank tank, Client client) {
        super(x, y, width, height);
        this.vel = vel;
        this.accuracy = accuracy;
        this.tank = tank; //The tank to follow
        setImage("house.png");
        houseStatus = true;
        this.client = client;
        houseFiring(100 - accuracy);
    }

    private double angle(double tankX, double tankY, double houseX, double houseY) {
        double opp = tankY - houseY;
        double adj = tankX - houseX;

        return 180 - Math.toDegrees(Math.atan2(adj, opp));
    }

    public void houseFiring(int accuracy) {
        while (!missile.isVisible()) {
            double dimX = (x - 10) + this.getWidth() / 2;
            double dimY = (y - 10) + this.getWidth() / 2;
            client.sendMessage("Bullet fired from house");
            missile = new Missile(dimX, dimY, vel, angle(tank.getX(), tank.getY(), this.getX(), this.getY()) - accuracy);
            missile.setImage("missileTank.png");
            missile.setVisible(true);
        }

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        while (true) {
            if (houseStatus) {
                houseFiring(10 - accuracy);
                houseStatus = true;
            }
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void setHouseStatus(boolean flag) {
        houseStatus = flag;
    }
}
