package A3_GAME;

import javax.swing.*;

public class EndGame implements Runnable {

    private static EndGame object;
    private int endCode = 7;
    private boolean flag = true;

    public static EndGame getInstance() {
        if (object == null) {
            object = new EndGame();
        }
        return object;
    }

    private EndGame() {

    }

    public void setEndCode(int endCode) {
        this.endCode = endCode;
    }

    @Override
    public void run() {
        while (flag) {
            if (endCode == 0) {
                JOptionPane.showMessageDialog(null, "YOU WON", "VICTORY!", JOptionPane.INFORMATION_MESSAGE);
                flag = false;
            } else if (endCode == 1) {
                JOptionPane.showMessageDialog(null, "HIT MINE", "YOU LOSE!", JOptionPane.INFORMATION_MESSAGE);
                flag = false;
            } else if (endCode == 2) {
                JOptionPane.showMessageDialog(null, "RAN OUT OF LIVES", "YOU LOSE!", JOptionPane.INFORMATION_MESSAGE);
                flag = false;
            } else
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

        }
        System.exit(1);
    }
}
