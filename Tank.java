package A3_GAME;

public class Tank extends Sprite implements Runnable {

    private boolean isAlive;
    public int orientation;
    private int vel;
    private int moveDir;
    private boolean firing;
    private boolean tankStatus;
    private int screenSizeH, screenSizeW;
    public Missile missile = new Missile(0, 0, 0, 0, false);
    private Client client;


    public Tank(double x, double y, int width, int height, Client client) {
        super(x, y, width, height);
        orientation = 4;
        vel = 15;
        isAlive = true;
        firing = false;
        moveDir = 0;
        tankStatus = false;
        setImage("tank.png");
        screenSizeH = Game.screenSizeH;
        screenSizeW = Game.screenSizeW;
        this.client = client;
    }

    public void updateTank() {
        if (firing) {
            if (!missile.isVisible()) {
                double dimX = (x - 10) + this.getWidth() / 2;
                double dimY = (y - 10) + this.getWidth() / 2;
                missile = new Missile(dimX, dimY, 15, orientation * 45);
                client.sendMessage("Bullet fired from Tank");
                missile.setImage("missileTank.png");
                missile.setVisible(true);
            }
            firing = false;
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } else if (moveDir == 0) {
            move(moveDir);
        } else if (moveDir == 2) {
            move(moveDir);
        } else if (moveDir == 1) {
            orientation = ((orientation + 1) % 8);
        } else if (moveDir == 3) {
            orientation = ((orientation + 7) % 8);
        }

    }

    //move:
    //0 -> forward
    //1 -> turn right
    //2 -> backward
    //3 -> turn left
    //The Thread here is setting all the variables
    // is the  Tabk Moving|rotating|firing?  whenever a key i pressed
    public void run(int move, boolean fire) {
        moveDir = move;
        if (fire)
            firing = true;


        tankStatus = true;
    }

    public void move(int dir) {
        double w;
        int flag = (dir == 0) ? 1 : -1;
        double previousX = x;
        double previousY = y;

        switch (orientation) {
            case 0: {
                y = y - flag * vel;
            }

            break;
            case 1: {
                w = Math.sin(Math.toRadians(45)) * vel * (-flag);
                x = x - w;
                y = y + w;

            }

            break;
            case 2: {
                x = x + flag * vel;
            }

            break;
            case 3: {
                w = Math.sin(Math.toRadians(45)) * vel * (flag);
                x = x + w;
                y = y + w;
            }

            break;
            case 4: {
                y = y + flag * vel;
            }

            break;
            case 5: {
                w = Math.sin(Math.toRadians(45)) * vel * (flag);
                x = x - w;
                y = y + w;
            }

            break;
            case 6: {
                x = x - flag * vel;
            }

            break;
            case 7: {
                w = Math.sin(Math.toRadians(45)) * vel * (-flag);
                x = x + w;
                y = y + w;
            }

            break;
        }

        if (this.intersects(Game.wall)) {
            x = previousX;
            y = previousY;
        }
        if (this.intersects(Game.house)) {
            x = previousX;
            y = previousY;
        }
        if (x < 0 || x + this.getWidth() * 1.2 > screenSizeW || y < 0 || y + this.getHeight() * 1.6 > screenSizeH) {
            x = previousX;
            y = previousY;
        }
    }

    @Override
    public void run() {
        while (isAlive) {
            //System.out.println("inside run tank@overide");
            if (tankStatus) {
                updateTank();
                tankStatus = false;
            }
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

    }

    public void setAlive(boolean flag) {
        isAlive = flag;
    }


}