package A3_GAME;

import javax.swing.*;
import java.awt.*;
import java.util.Hashtable;


public class Settings {

    private static volatile boolean didSubmit = false;

    public static void InitializeGame() {

        // Create and set up a frame window
        JFrame.setDefaultLookAndFeelDecorated(false);
        JFrame mainFrame = new JFrame("Settings");
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.setSize(500, 500);
        mainFrame.setLayout(new GridLayout(4, 1));


        // Set the panel to add buttons
        JPanel panel = new JPanel();
        JPanel panel2 = new JPanel();
        JPanel panel3 = new JPanel();

        // Different settings on the sliders
        JSlider slider = new JSlider();
        JSlider slider2 = new JSlider();
        JSlider slider3 = new JSlider();


        // Set major or minor ticks for the slider
        slider.setMajorTickSpacing(25);
        slider.setMinorTickSpacing(10);
        slider.setPaintTicks(true);
        slider.setPaintLabels(true);
        slider.setValue(15);

        slider2.setMajorTickSpacing(10);
        slider2.setMinorTickSpacing(10);
        slider2.setPaintTicks(true);
        slider2.setPaintLabels(true);
        slider2.setValue(20);

        slider3.setMajorTickSpacing(25);
        slider3.setMinorTickSpacing(10);
        slider3.setPaintTicks(true);
        slider3.setPaintLabels(true);
        slider3.setValue(100);

        // Add positions label in the slider
        Hashtable<Integer, JLabel> position = new Hashtable<Integer, JLabel>();
        position.put(0, new JLabel("0"));
        position.put(25, new JLabel("25"));
        position.put(50, new JLabel("50"));
        position.put(75, new JLabel("75"));
        position.put(100, new JLabel("100"));


        Hashtable<Integer, JLabel> position2 = new Hashtable<>();
        position2.put(0, new JLabel("0"));
        position2.put(20, new JLabel("1"));
        position2.put(40, new JLabel("2"));
        position2.put(60, new JLabel("3"));
        position2.put(80, new JLabel("4"));
        position2.put(100, new JLabel("5"));

        // Set the label to be drawn
        slider.setLabelTable(position);
        slider2.setLabelTable(position2);
        slider3.setLabelTable(position);

        // Add the slider to the panel
        panel.add(slider);
        panel2.add(slider2);
        panel3.add(slider3);


        JLabel status = new JLabel("Slide to select house fire Rate", JLabel.CENTER);
        JLabel status2 = new JLabel("Slide to select number of mines", JLabel.CENTER);
        JLabel status3 = new JLabel("Slide to select accuracy of enemy fire", JLabel.CENTER);

        slider.addChangeListener(e -> status.setText("House Fire Rate: " + ((JSlider) e.getSource()).getValue()));
        slider2.addChangeListener(e -> status2.setText("Number of Mines: " + ((JSlider) e.getSource()).getValue() / 20));
        slider3.addChangeListener(e -> status3.setText("Accuracy: " + ((JSlider) e.getSource()).getValue()));


        JButton submit = new JButton("Submit");
        submit.addActionListener(e -> didSubmit = true);


        // Set the window to be visible as the default to be false
        mainFrame.add(panel);
        mainFrame.add(status);
        mainFrame.add(panel2);
        mainFrame.add(status2);
        mainFrame.add(panel3);
        mainFrame.add(status3);
        mainFrame.add(submit, BorderLayout.SOUTH);
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setVisible(true);
        mainFrame.pack();

        //Loop to wait for submit button to be pressed
        while (!didSubmit) {
            Thread.onSpinWait();
        }
        Game.runGame(slider.getValue(), slider2.getValue() / 20, slider3.getValue() / 10);
        mainFrame.dispose();
    }

}