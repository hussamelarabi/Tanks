package A3_GAME;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Random;

public class Game extends JPanel implements KeyListener, Runnable {


    private Client client;
    static int screenSizeH, screenSizeW;
    private Explosion explosion;
    private Tank tank;
    private Thread tankThread;
    public Mine[] mine;
    public static Wall wall;
    public static House house;
    private boolean gameStatus = false;
    private int tankLifes = 3, houseLifes = 3, mineCount;
    private EndGame end;


    private Game(int fireVel, int mineCount, int accuracy) {
        this.mineCount = mineCount;
        client = new Client();

        addKeyListener(this);
        setFocusable(true);
        screenSizeW = 700;
        screenSizeH = 700;


        wall = new Wall(140, 140, 50, 50);
        tank = new Tank(20, 20, 50, 50, client);
        new Thread(this).start(); //Thread goes to Game(run)
        tankThread = new Thread(tank);
        tankThread.setName("tankThread");
        tankThread.start();
        house = new House(500, 500, 50, 50, fireVel, accuracy, tank, client);


        Random random = new Random();
        mine = new Mine[5];
        mine[0] = new Mine(1, 1, 50, 50);
        mine[1] = new Mine(1, 1, 50, 50);
        mine[2] = new Mine(1, 1, 50, 50);
        mine[3] = new Mine(1, 1, 50, 50);
        mine[4] = new Mine(1, 1, 50, 50);
        for (int i = 0; i < mineCount; i++) {

            mine[i].setX(random.nextInt(600) + 50);
            mine[i].setY(random.nextInt(600) + 50);
            mine[i].setVisible(true);
            while (mine[i].intersects(tank) || mine[i].intersects(house) || mine[i].intersects(wall)) {
                mine[i].setX(random.nextInt(600) + 50);
                mine[i].setY(random.nextInt(600) + 50);
            }

        }


        end = EndGame.getInstance();


        new Thread(house).start();
        new Thread(end).start();


    }


    static void runGame(int fireVel, int mineCount, int accuracy) {
        JFrame f = new JFrame();
        f.setTitle("TANKS!");
        f.add(new Game(fireVel, mineCount, accuracy));
        f.setSize(screenSizeW, screenSizeH);    // Set the frame size
        f.setLocationRelativeTo(null);         //center the frame
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.getRootPane().setBorder(BorderFactory.createMatteBorder(4, 4, 4, 4, Color.BLACK));
        f.setVisible(true);        //Make your frame visible
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        int code = e.getKeyCode();
        //Check if tank will hit a tank if it will prevent if
        move_tank(tank, code);
        gameStatus = true;
    }


    @Override
    public void keyReleased(KeyEvent e) {
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(Color.ORANGE);
        g2d.fillRect(0, 0, getWidth(), getHeight());


        if (explosion != null)
            if (explosion.isVisible()) {
                g.drawImage(explosion.getImage(), (int) explosion.getX(), (int) explosion.getY(), (int) explosion.getWidth(), (int) explosion.getHeight(), null);
            }

        for (int i = 0; i < mineCount; i++)
            if (mine[i] != null)
                if (mine[i].isVisible())
                    g.drawImage(mine[i].getImage(), (int) mine[i].getX(), (int) mine[i].getY(), (int) mine[i].getWidth(), (int) mine[i].getHeight(), null);

        if (wall.isVisible()) {
            g2d.drawImage(wall.getImage(), (int) wall.getX(), (int) wall.getY(), (int) wall.getWidth(), (int) wall.getHeight(), null);
        }

        //HOUSE
        Missile h = house.missile;
        if (h.isVisible() && (((int) h.getX() <= 0) || ((int) h.getX() >= screenSizeW)
                || h.y <= 0 || h.y >= screenSizeH || h.intersects(wall))) {
            h.setVisible(false);
        }
        if (h.isVisible() && h.intersects(tank)) {
            h.setVisible(false);
            System.out.println("Tank Lives: " + --tankLifes);
        }

        if (h.isVisible()) {
            g.drawImage(h.getImage(), (int) h.x, (int) h.y, (int) h.getWidth(), (int) h.getHeight(), this);
        }
        if (house.isVisible()) {
            g2d.drawImage(house.getImage(), (int) house.getX(), (int) house.getY(), (int) house.getWidth(), (int) house.getHeight(), null);
        }
        //Tank
        Missile m = tank.missile;
        if (m.isVisible() && (((int) m.getX() <= 0) || ((int) m.getX() >= screenSizeH) || m.y <= 0 || m.y >= 900)
                || m.y <= 0 || m.y >= screenSizeH || m.intersects(wall))
            m.setVisible(false);

        if (m.isVisible() && m.intersects(house)) {
            m.setVisible(false);
            System.out.println("House Lives: " + --houseLifes);
        }

        if (m.isVisible()) {
            g.drawImage(m.getImage(), (int) m.x, (int) m.y, (int) m.getWidth(), (int) m.getHeight(), this);
        }

        if (tank.isVisible()) {
            g2d.rotate(Math.toRadians(tank.orientation * 45), tank.getX() + tank.getWidth() / 2, tank.getY() + tank.getHeight() / 2);
            g2d.drawImage(tank.getImage(), (int) tank.getX(), (int) tank.getY(), (int) tank.getWidth(), (int) tank.getHeight(), null);
        }
    }

    @Override
    public void run() {
        while (true) {
            if (gameStatus) {
                repaint();
            }

            if (houseLifes == 0) {
                explosion = new Explosion(house.getX(), house.getY(), 50, 50, true);
                house.setVisible(false);
                house.setHouseStatus(false);
                tank.setAlive(false);
                repaint();
                gameStatus = false;
                end.setEndCode(0);
            }
            if (tankLifes == 0) {
                explosion = new Explosion(tank.getX(), tank.getY(), 50, 50, true);
                tank.setVisible(false);
                tank.setAlive(false);
                house.setHouseStatus(false);
                repaint();
                gameStatus = false;
                end.setEndCode(2);
            }


            for (int i = 0; i < mineCount; i++)
                try {
                    if (tank_intersects_mine(tank, mine[i])) {
                        explosion = new Explosion(mine[i].getX(), mine[i].getY(), 50, 50, true);
                        tank.setVisible(false);
                        tank.setAlive(false);
                        mine[i].setVisible(false);
                        house.setHouseStatus(false);
                        repaint();
                        gameStatus = false;
                        end.setEndCode(1);
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean tank_intersects_mine(Tank t, Mine m) {

        return t.intersects(m);
    }


    //move tank depending on the input key
    private void move_tank(Tank t, int code) {
        if (code == KeyEvent.VK_UP) {
            //System.out.println("UP--");
            t.run(0, false);
        } else if (code == KeyEvent.VK_RIGHT) {
            // System.out.println("RIGHT--");
            t.run(1, false);
        } else if (code == KeyEvent.VK_DOWN) {
            // System.out.println("DOWN--");
            t.run(2, false);
        } else if (code == KeyEvent.VK_LEFT) {
            // System.out.println("LEFT--");
            t.run(3, false);
        } else if (code == KeyEvent.VK_SPACE) {
            // System.out.println("Fire--");
            t.run(0, true);
        }
    }


}
