package A3_GAME;

public class Wall extends Sprite {
    public Wall(double x, double y, int width, int height) {
        super(x, y, width, height);
        setImage("wall.png");
    }

}
