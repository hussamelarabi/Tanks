package A3_GAME;


class Mine extends Sprite {

    Mine(double x, double y, int width, int height) {
        super(x, y, width, height);
        this.setImage("mine.png");
    }
}